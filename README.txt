1. buat akun di gitlab/github

2. install gitbash untuk windows

3. create project di gitlab

4. clone projects
	$ git clone https://gitlab.com/samsulhadi/chattkkom.git
5. masuk ke direktori projects nya 
	$ cd chattkkom
6. check current url repository 
	$ git remote -v
7. ubah url project ke project baru anda di gitlab
	$ git remote set-url origin https://gitlab.com/<username>/<projectname>.git

8. check status repository
	$ git status
9. upload/update ke server
	$ git add -A
	$ git commit -am "<informasi pesan perubahan>"
	$ git push origin master
10. update repository local
	$ git pull
	
