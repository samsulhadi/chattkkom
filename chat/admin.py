from django.contrib import admin
from chat.models import GroupChat, Pesan
from django.utils.safestring import mark_safe

# Register your models here.

admin.site.register(Pesan)

class GroupChatAdmin(admin.ModelAdmin):
	list_display = ('chat', 'keterangan', 'aksi')

	def keterangan(self, obj):
		jumlah_anggota = obj.anggota.all().count()
		if jumlah_anggota < 4 and jumlah_anggota > 0:
			return ", ".join(u.username for u in obj.anggota.all())
		return str(jumlah_anggota)+" user"

	def chat(self, obj):
		if obj.nama_group:
			return obj.nama_group
		else:
			return "-"
	chat.short_description = 'Daftar Chatting'

	def aksi(self, obj):
		return mark_safe("<a href='#' onclick='return alert(\""+obj.nama_group+"\")'>Lihat Chat</a>")

admin.site.register(GroupChat, GroupChatAdmin)
