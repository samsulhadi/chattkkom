from django.shortcuts import render

# Create your views here.

def welcome(request, extra_context={}):
	nama = "Ubaya"
	
	extra_context.update({'nama': nama})

	return render(request, "welcome.html", extra_context)