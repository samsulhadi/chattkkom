# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0002_pesan'),
    ]

    operations = [
        migrations.AddField(
            model_name='pesan',
            name='dibuat_pada',
            field=models.DateTimeField(default=datetime.datetime(2018, 2, 26, 8, 1, 4, 21755, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
